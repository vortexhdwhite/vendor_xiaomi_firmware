# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi/firmware/venus/abl.img:install/firmware-update/abl.img \
    vendor/xiaomi/firmware/venus/aop.img:install/firmware-update/aop.img \
    vendor/xiaomi/firmware/venus/bluetooth.img:install/firmware-update/bluetooth.img \
    vendor/xiaomi/firmware/venus/cpucp.img:install/firmware-update/cpucp.img \
    vendor/xiaomi/firmware/venus/devcfg.img:install/firmware-update/devcfg.img \
    vendor/xiaomi/firmware/venus/dsp.img:install/firmware-update/dsp.img \
    vendor/xiaomi/firmware/venus/featenabler.img:install/firmware-update/featenabler.img \
    vendor/xiaomi/firmware/venus/hyp.img:install/firmware-update/hyp.img \
    vendor/xiaomi/firmware/venus/imagefv.img:install/firmware-update/imagefv.img \
    vendor/xiaomi/firmware/venus/keymaster.img:install/firmware-update/keymaster.img \
    vendor/xiaomi/firmware/venus/modem.img:install/firmware-update/modem.img \
    vendor/xiaomi/firmware/venus/qupfw.img:install/firmware-update/qupfw.img \
    vendor/xiaomi/firmware/venus/shrm.img:install/firmware-update/shrm.img \
    vendor/xiaomi/firmware/venus/tz.img:install/firmware-update/tz.img \
    vendor/xiaomi/firmware/venus/uefisecapp.img:install/firmware-update/uefisecapp.img \
    vendor/xiaomi/firmware/venus/vm-bootsys.img:install/firmware-update/vm-bootsys.img \
    vendor/xiaomi/firmware/venus/xbl.img:install/firmware-update/xbl.img \
    vendor/xiaomi/firmware/venus/xbl_config.img:install/firmware-update/xbl_config.img